class AddOrganizationIdToActivations < ActiveRecord::Migration
  def change
    add_column :activations, :organization_id, :uuid
  end
end

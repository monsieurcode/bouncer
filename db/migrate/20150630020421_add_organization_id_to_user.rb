class AddOrganizationIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :organization_id, :uuid
  end
end

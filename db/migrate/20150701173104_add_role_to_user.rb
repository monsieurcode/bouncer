class AddRoleToUser < ActiveRecord::Migration
  def change
    add_column :users, :role, :integer, default: 0  # 0 cooresponds to the role 'customer'
  end
end

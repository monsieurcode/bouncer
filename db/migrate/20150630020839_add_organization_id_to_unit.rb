class AddOrganizationIdToUnit < ActiveRecord::Migration
  def change
    add_column :units, :organization_id, :uuid
  end
end

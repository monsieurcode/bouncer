FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.cell_phone }
    password { Faker::Internet.password(8) }
    email_verified_at { Time.now.utc }
    phone_verified_at { Time.now.utc }

    factory :customer do
      role 'customer'
    end

    factory :attendant do
      organization
      role 'attendant'
    end

    factory :manager do
      organization
      role 'manager'
    end

    factory :admin do
      organization
      role 'admin'
    end

    factory :super_admin do
      organization
      super_admin true
      role 'super_admin'
    end

    factory :unconfirmed_user do
      email_verified_at nil
      phone_verified_at nil
    end
  end
end


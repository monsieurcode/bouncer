require 'request_helper'

describe 'User request' do
  let(:org) { FactoryGirl.create(:organization) }

  let!(:super_admin) { FactoryGirl.create(:super_admin, organization: org) }
  let!(:admin) { FactoryGirl.create(:admin, organization: org) }
  let!(:manager) { FactoryGirl.create(:manager, organization: org) }
  let!(:attendant) { FactoryGirl.create(:attendant, organization: org) }
  let!(:customer) { FactoryGirl.create(:user) }

  let(:customer_token) { FactoryGirl.create(:access_token, resource_owner_id: customer.id).token }
  let(:attendant_token) { FactoryGirl.create(:access_token, resource_owner_id: attendant.id).token }
  let(:manager_token) { FactoryGirl.create(:access_token, resource_owner_id: manager.id).token }
  let(:admin_token) { FactoryGirl.create(:access_token, resource_owner_id: admin.id).token }
  let(:super_admin_token) { FactoryGirl.create(:admin_access_token, resource_owner_id: super_admin.id).token }

  let(:user_payload) {{
    users: {
      email: Faker::Internet.email,
      password: '123456789',
      password_confirmation: '123456789'
    }
  }}

  describe 'GET /users' do
    context 'authorization' do
      it 'allows super_admins to list users' do
        get users_path(format: :json), access_token: super_admin_token
        expect(response).to be_success
      end

      it 'allows admins to list users' do
        get users_path(format: :json), access_token: admin_token
        expect(response).to be_success
      end

      it 'returns 403 forbidden for managers' do
        get users_path(format: :json), access_token: manager_token
        expect(response.status).to eq 403
      end

      it 'returns 403 forbidden for attendants' do
        get users_path(format: :json), access_token: attendant_token
        expect(response.status).to eq 403
      end

      it 'returns 403 forbidden for customers' do
        get users_path(format: :json), access_token: customer_token
        expect(response.status).to eq 403
      end
    end

    context 'tenancy' do
      it 'displays all users when the current user is a super_admin' do
        get users_path(format: :json), access_token: super_admin_token
        expect(json['users'].count).to eq User.count
      end

      it 'only shows users within the same organization for admins' do
        get users_path(format: :json), access_token: admin_token
        expect(json['users'].count).to eq org.users.count
      end
    end
  end
end

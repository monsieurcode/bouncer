require 'request_helper'

describe 'Organization Requests' do
  let!(:org) { FactoryGirl.create(:organization) }
  let!(:org2) { FactoryGirl.create(:organization) }
  let(:user) { FactoryGirl.create(:user) }
  let(:user_token) { FactoryGirl.create(:access_token, resource_owner_id: user.id).token }

  describe 'GET /organizations' do
    context 'when user is super_admin' do
      before do
        user.update_attributes({
          super_admin: true,
          role: 'super_admin'
        })
        FactoryGirl.create_list(:organization, 3)
      end

      it 'should list all organizations' do
        get organizations_path(format: :json), access_token: user_token
        expect(response).to be_success
        expect(json['organizations'].count).to eq 5
      end
    end

    context 'when user is admin' do
      before do
        user.update_attributes({
          role: 'admin'
        })
      end

      it 'should return 401 unauthorized' do
        get organizations_path(format: :json), access_token: user_token
        expect(response.status).to eq 403
      end
    end

    context 'when user is manager' do
      before do
        user.update_attributes({
          role: 'manager'
        })
      end

      it 'should return 401 unauthorized' do
        get organizations_path(format: :json), access_token: user_token
        expect(response.status).to eq 403
      end
    end

    context 'when user is attendant' do
      before do
        user.update_attributes({
          role: 'attendant'
        })
      end

      it 'should return 401 unauthorized' do
        get organizations_path(format: :json), access_token: user_token
        expect(response.status).to eq 403
      end
    end

    context 'when user is customer' do
      before do
        user.update_attributes({
          role: 'customer'
        })
      end

      it 'should return 401 unauthorized' do
        get organizations_path(format: :json), access_token: user_token
        expect(response.status).to eq 403
      end
    end
  end

  describe 'GET /organizations/:id' do

    context 'when the user is super_admin' do
      before do
        user.update_attributes({
          super_admin: true,
          role: 'super_admin',
          organization: org
        })
      end

      it 'should allow the user to view his current organization' do
        get organization_path(org, format: :json), access_token: user_token
        expect(response).to be_success
        expect(json['organizations'].count).to eq 1
        expect(json['organizations'][0]['id']).to eq org.id
      end

      it 'should allow the user to view an organization that it is not it' do
        get organization_path(org2, format: :json), access_token: user_token
        expect(response).to be_success
        expect(json['organizations'].count).to eq 1
        expect(json['organizations'][0]['id']).to eq org2.id
      end
    end

    context 'when the user is admin' do
      before do
        user.update_attributes({
          role: 'admin',
          organization: org
        })
      end

      it 'should allow the user to view his current organization' do
        get organization_path(org, format: :json), access_token: user_token
        expect(response).to be_success
        expect(json['organizations'].count).to eq 1
        expect(json['organizations'][0]['id']).to eq org.id
      end

      it 'should not allow the user to view organizations it is not a part of' do
        get organization_path(org2, format: :json), access_token: user_token
        expect(response.status).to eq 404
      end
    end

    context 'when the user is manager' do
      before do
        user.update_attributes({
          role: 'manager',
          organization: org
        })
      end

      it 'should allow the user to view his current organization' do
        get organization_path(org, format: :json), access_token: user_token
        expect(response).to be_success
        expect(json['organizations'].count).to eq 1
        expect(json['organizations'][0]['id']).to eq org.id
      end

      it 'should not allow the user to view organizations it is not a part of' do
        get organization_path(org2, format: :json), access_token: user_token
        expect(response.status).to eq 404
      end
    end

    context 'when the user is attendant' do
      before do
        user.update_attributes({
          role: 'attendant',
          organization: org
        })
      end

      it 'should allow the user to view his current organization' do
        get organization_path(org, format: :json), access_token: user_token
        expect(response).to be_success
        expect(json['organizations'].count).to eq 1
        expect(json['organizations'][0]['id']).to eq org.id
      end

      it 'should not allow the user to view organizations it is not a part of' do
        get organization_path(org2, format: :json), access_token: user_token
        expect(response.status).to eq 404
      end
    end

    context 'when the user is customer' do
      before do
        user.update_attributes({
          role: 'customer',
        })
      end

      it 'should not allow viewing of any company' do
        get organization_path(org, format: :json), access_token: user_token
        expect(response.status).to eq 404
      end

      it 'should not allow the user to view organizations it is not a part of' do
        get organization_path(org2, format: :json), access_token: user_token
        expect(response.status).to eq 404
      end
    end
  end

  describe 'PUT /organizations/:id' do
    let(:payload) {{
      organizations: {
        name: 'changed'
      },
      access_token: user_token
    }}

    context 'when the user is a super_admin' do
      before do
        user.update_attributes({
          super_admin: true,
          role: 'super_admin',
          organization: org
        })
      end

      it 'should allow the user to update an organization it is in' do
        put organization_path(org, format: :json), payload
        expect(response.status).to eq 204
        expect(org.reload.name).to eq 'changed'
      end

      it 'should allow the user to update an organization it is not in' do
        put organization_path(org, format: :json), payload
        expect(response.status).to eq 204
        expect(org.reload.name).to eq 'changed'
      end
    end

    context 'when the user is an admin' do
      before do
        user.update_attributes({
          role: 'admin',
          organization: org
        })
      end

      it 'should allow the user to update an organization it is in' do
        put organization_path(org, format: :json), payload
        expect(response.status).to eq 204
        expect(org.reload.name).to eq 'changed'
      end

      it 'should not allow the user to update an organization it is not in' do
        put organization_path(org2, format: :json), payload
        expect(response.status).to eq 404
        expect(org2.reload.name).to_not eq 'changed'
      end
    end

    context 'when the user is an manager' do
      before do
        user.update_attributes({
          role: 'manager',
          organization: org
        })
      end

      it 'should not allow the user to update an organization it is in' do
        put organization_path(org, format: :json), payload
        expect(response.status).to eq 403
        expect(org.reload.name).to_not eq 'changed'
      end

      it 'should not allow the user to update an organization it is not in' do
        put organization_path(org2, format: :json), payload
        expect(response.status).to eq 404
        expect(org2.reload.name).to_not eq 'changed'
      end
    end

    context 'when the user is an attendant' do
      before do
        user.update_attributes({
          role: 'attendant',
          organization: org
        })
      end

      it 'should not allow the user to update an organization it is in' do
        put organization_path(org, format: :json), payload
        expect(response.status).to eq 403
        expect(org.reload.name).to_not eq 'changed'
      end

      it 'should not allow the user to update an organization it is not in' do
        put organization_path(org2, format: :json), payload
        expect(response.status).to eq 404
        expect(org2.reload.name).to_not eq 'changed'
      end
    end

    context 'when the user is a customer' do
      before do
        user.update_attributes({
          role: 'customer'
        })
      end

      it 'should not allow the user to update an organization' do
        put organization_path(org, format: :json), payload
        expect(response.status).to eq 404
        expect(org.reload.name).to_not eq 'changed'
      end
    end
  end

  describe 'POST /organizations' do
    let(:payload) {{
      organizations: {
        name: 'new'
      },
      access_token: user_token
    }}

    context 'when the user is a super_admin' do
      before do
        user.update_attributes({
          super_admin: true,
          role: 'super_admin'
        })
      end

      it 'should allow the user to create an organization' do
        expect{ post organizations_path(format: :json), payload }.to change{ Organization.count }.by 1
        expect(response).to be_success
        expect(json['organizations'][0]['name']).to eq 'new'
      end
    end

    context 'when the user is an admin' do
      before do
        user.update_attributes({
          role: 'admin'
        })
      end

      it 'should not allow the user to create an organization' do
        expect{ post organizations_path(format: :json), payload }.to_not change{ Organization.count }
        expect(response.status).to eq 403
      end
    end

    context 'when the user is an manager' do
      before do
        user.update_attributes({
          role: 'manager'
        })
      end

      it 'should not allow the user to create an organization' do
        expect{ post organizations_path(format: :json), payload }.to_not change{ Organization.count }
        expect(response.status).to eq 403
      end
    end

    context 'when the user is an attendant' do
      before do
        user.update_attributes({
          role: 'attendant'
        })
      end

      it 'should not allow the user to create an organization' do
        expect{ post organizations_path(format: :json), payload }.to_not change{ Organization.count }
        expect(response.status).to eq 403
      end
    end

    context 'when the user is a customer' do
      before do
        user.update_attributes({
          role: 'customer'
        })
      end

      it 'should not allow the user to create an organization' do
        expect{ post organizations_path(format: :json), payload }.to_not change{ Organization.count }
        expect(response.status).to eq 403
      end
    end
  end

  describe 'DELETE /organizations/:id' do
    context 'when the user is a super_admin' do
      before do
        user.update_attributes({
          super_admin: true,
          role: 'super_admin'
        })
      end

      it 'deletes the organization' do
        expect{delete organization_path(org, format: :json), access_token: user_token}.to change{Organization.count}
        expect(response.status).to eq 204
      end
    end

    context 'when the user is a admin' do
      before do
        user.update_attributes({
          super_admin: false,
          role: 'admin'
        })
      end

      it 'does not delete the organization' do
        expect{delete organization_path(org, format: :json), access_token: user_token}.to_not change{Organization.count}
        expect(response.status).to eq 404
      end
    end

    context 'when the user is a manager' do
      before do
        user.update_attributes({
          super_admin: false,
          role: 'manager'
        })
      end

      it 'does not delete the organization' do
        expect{delete organization_path(org, format: :json), access_token: user_token}.to_not change{Organization.count}
        expect(response.status).to eq 404
      end
    end

    context 'when the user is a attendant' do
      before do
        user.update_attributes({
          super_admin: false,
          role: 'attendant'
        })
      end

      it 'does not delete the organization' do
        expect{delete organization_path(org, format: :json), access_token: user_token}.to_not change{Organization.count}
        expect(response.status).to eq 404
      end
    end

    context 'when the user is a customer' do
      before do
        user.update_attributes({
          super_admin: false,
          role: 'customer'
        })
      end

      it 'does not delete the organization' do
        expect{delete organization_path(org, format: :json), access_token: user_token}.to_not change{Organization.count}
        expect(response.status).to eq 404
      end
    end
  end
end

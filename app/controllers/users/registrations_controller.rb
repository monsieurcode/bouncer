class Users::RegistrationsController < Devise::RegistrationsController
  respond_to :json
  def create
    user = User.new(user_params.except(:links))
    user = update_links user

    if user.save
      render_json_api user
    else
      render_errors_for user
    end
  end

  private

  def user_params
    params.require(:users).permit(:email, :name, :phone, :password, :password_confirmation, :role, links: [:organization])
  end

  def update_links(user)
    if user_params[:links]
      user.organization_id = user_params[:links][:organization]
    end
    user
  end
end

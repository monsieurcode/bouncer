class UnitsController < ApplicationController
  before_action :authenticate!

  def index
    @units = Unit.page(page).per(params[:per_page])
    render json: @units
  end

  def show
    @unit = Unit.find(params[:id])
    render json: @unit
  end

  def create
    authenticate_device_scope_or_admin!
    @unit = Unit.new

    @unit.serial = params['units']['serial']

    if @unit.save
      render json: @unit
    else
      render_errors_for @unit
    end
  end
end

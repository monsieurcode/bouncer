class OrganizationsController < ApplicationController
  before_action :authenticate!
  before_action :set_org, only: [:show, :update, :destroy]
  after_action :verify_authorized

  def index
    authorize @organizations = Organization.page(page).per(params['per_page'])
    render_json_api @organizations
  end

  def show
    render_json_api @organization
  end

  def update
    authorize @organization = update_model(@organization)

    if @organization.save
      head :no_content
    else
      render_errors_for @organization
    end
  end

  def create
    authorize @organization = Organization.new(org_params)

    if @organization.save
      render_json_api @organization
    else
      render_errors_for @organization
    end
  end

  def destroy
    @organization.delete
    head :no_content
  end

  protected

  def set_org
    authorize @organization = policy_scope(Organization).find(params['id'])
  end

  def org_params
    params.require(:organizations).permit(:name)
  end

  def update_model org
    name = org_params[:name]

    org.name = name if name
    org
  end
end

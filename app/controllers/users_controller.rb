class UsersController < ApplicationController
  respond_to :json

  def index
    authenticate!
    @users = policy_scope(User).page(page).per(params[:per_page])
    authorize @users
    render_json_api @users
  end

  def confirm
    @user = User.find(params[:user_id])

    if params[:confirmation_token] && params[:confirmation_token] == @user.email_confirmation_token
      @user.confirm_email!
      head :no_content
    elsif params[:confirmation_token] && params[:confirmation_token] == @user.phone_verification_code
      @user.confirm_phone!
      head :no_content
    else
      authenticate_user!
      raise Pundit::NotAuthorizedError unless current_user.super_admin? || @user.id == current_user.id
      @user.reset_confirmation!
      head :no_content
    end
  end

  def show
    authenticate!
    if params[:id] == 'me' && current_user
      @user = current_user
    else
      @user = User.find(params[:id])
    end

    authorize @user
    render_json_api @user
  end

  def update
    authenticate!
    if params[:id] == 'me' || params[:id].nil?
      user = current_user
    elsif params[:id] == current_user.id || current_user.super_admin?
      user = User.find(params[:id])
    else
      raise Pundit::NotAuthorizedError
    end
    user.phone = user_params['phone'] if user_params["phone"]
    user.email = user_params['email'] if user_params["email"]
    user.name = user_params["name"] if user_params["name"]
    user.role = user_params['role'] if user_params['role']
    user.organization_id = user_params['links']['organization'] if user_params['links'] && user_params['links']['organization']
    if user.save
      render_json_api user
    else
      render_errors_for user
    end
  end

  def admin
    authenticate!
    @user = User.find(params[:user_id])
    @user.super_admin = true
    authorize @user
    if @user.save
      flash[:notice] = "Admin granted"
    else
      flash[:error] = "Could not grant admin"
    end
    render_json_api @user
  end

  def destroy
    authenticate!
    authorize user = User.find(params[:id])
    user.delete
    head :no_content
  end

  private

  def can_show user
    return true if current_service == 'cashier' || current_service == 'barback'
    return true if current_user && current_user.super_admin?
    return true if current_user && user.id == current_user.id
    false
  end

  def user_params
    return params['user'] if params['user'].present?
    return params['users'] if params['users'].present?
    raise BadRequestError
  end
end

class OrgScopedObjectPolicy < ApplicationPolicy
  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
      ensure_user
    end

    def resolve
      return scope if user.super_admin?
      scope.where("#{id_field} = ?", user.organization_id)
    end

    def id_field
      scope == Organization ? 'id' : 'organization_id'
    end

    def ensure_user
      # create stub user if current_user is another API service
      if user.id == '00000000-0000-0000-0000-000000000000'
        @user = User.new(role: 'super_admin')
      end
    end
  end
end

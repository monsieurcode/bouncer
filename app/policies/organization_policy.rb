class OrganizationPolicy < OrgScopedObjectPolicy
  def index?
    user.super_admin?
  end

  def show?
    not user.customer?
  end

  def update?
    admin_type?
  end

  def create?
    user.super_admin?
  end

  def destroy?
    user.super_admin?
  end
end

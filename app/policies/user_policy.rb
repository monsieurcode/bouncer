class UserPolicy < OrgScopedObjectPolicy
  def edit?
    false
  end

  def new?
    false
  end

  def confirm?
    # anyone can confirm because we have custom authorization on that action
    true
  end

  def show?
    user.role == 'super_admin' || user.id == record.id
  end

  def admin
    admin_type?
  end

  def destroy?
    user.role == 'super_admin'
  end
end

class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
    ensure_user
  end

  def index?
    admin_type?
  end

  def show?
    scope.where(:id => record.id).exists?
  end

  def create?
    admin_type?
  end

  def new?
    admin_type?
  end

  def update?
    admin_type?
  end

  def edit?
    update?
  end

  def destroy?
    admin_type?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def admin_type?
    user.super_admin? || user.admin?
  end

  def ensure_user
    # create stub user if current_user is another API service
    if user.id == '00000000-0000-0000-0000-000000000000'
      @user = User.new(role: 'super_admin')
    end
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end

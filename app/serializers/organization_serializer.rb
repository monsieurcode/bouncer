class OrganizationSerializer < JsonApiSerializer
  attributes :id, :name, :updated_at, :created_at
  attributes :links

  def links
    {
      users: object.users.map(&:id)
    }
  end
end

class Organization < ActiveRecord::Base
  default_scope ->{ order(:name) }
  has_many :users
  has_many :units
  has_many :activations
end
